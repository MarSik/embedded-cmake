# Build time configuration dir

This directory contains definitions that CMake needs
when building the project using the selected toolchain.

The file MUST be included in the main CMakeLists.txt
file using the following command:

```
TOOLCHAIN_PREPARE()
```

