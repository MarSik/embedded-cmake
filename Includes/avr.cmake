ENABLE_LANGUAGE(C CXX ASM)

# Set the clock frequency if it is not defined
IF(F_CPU)
SET(F_CPU ${F_CPU} CACHE STRING "Clock frequency")
ELSE(F_CPU)
MESSAGE(SEND_ERROR "Please set the clock frequency to F_CPU variable.")
ENDIF(F_CPU)

SET_PROPERTY(TARGET ${PROJECT_NAME} APPEND PROPERTY SOURCES ${TOOLCHAIN_SOURCES})
SET_PROPERTY(TARGET ${PROJECT_NAME} APPEND_STRING PROPERTY COMPILE_FLAGS " -DF_CPU=${F_CPU} -mmcu=${CPU}")
SET_PROPERTY(TARGET ${PROJECT_NAME} APPEND_STRING PROPERTY LINK_FLAGS " -mmcu=${CPU} -Wl,-Map=${PROJECT_NAME}.map")

SET(OBJCOPY_HEX_FLAGS "-R" ".fuse" "-R" ".eeprom")
SET(OBJCOPY_BIN_FLAGS "-R" ".fuse" "-R" ".eeprom")

# Target to prepare an EEPROM image
ADD_CUSTOM_TARGET(${PROJECT_NAME}.eep DEPENDS ${PROJECT_NAME} COMMAND "${CMAKE_OBJCOPY} -Oihex -j .eeprom --set-section-flags=.eeprom=\"alloc,load\" --change-section-lma .eeprom=0 ${PROJECT_NAME} ${PROJECT_NAME}.eep")

INCLUDE("${CMAKE_CURRENT_LIST_DIR}/embedded.cmake")

