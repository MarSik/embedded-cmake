# Define variables used in the common family cmake script
SET(ARM_C_FLAGS "${ARM_C_FLAGS} -D${CPU} -D__FPU_PRESENT=1")


INCLUDE("${CMAKE_CURRENT_LIST_DIR}/arm-none-eabi.cmake")

