# Define variables used in the linker script
SET(FLASH_SIZE_KB 256 CACHE STRING "Flash size in KiB")
SET(RAM_SIZE_KB 48 CACHE STRING "RAM size in KiB")
SET(CCM_SIZE_B 8 CACHE STRING "CCM RAM size in KiB")

INCLUDE("${CMAKE_CURRENT_LIST_DIR}/arm-none-eabi.cmake")

