# - Try to find Arduino
# Once done this will define
#  Arduino_FOUND - There is Arduino in the library path
#  Arduino_INCLUDE_DIRS - Arduino include directories
#  Arduino_SOURCES
#  Arduino_LIBRARY_DIR - Arduino internal libraries

# Arduino_FIND_COMPONENTS = [core variant], default
IF(DEFINED Arduino_FIND_COMPONENTS)
LIST(LENGTH Arduino_FIND_COMPONENTS COMPONENTS_SIZE)
ENDIF(DEFINED Arduino_FIND_COMPONENTS)

# Get the core name
IF(DEFINED COMPONENTS_SIZE AND COMPONENTS_SIZE GREATER 0)
  LIST(GET Arduino_FIND_COMPONENTS 0 CORE)
ELSE(DEFINED COMPONENTS_SIZE AND COMPONENTS_SIZE GREATER 0)
  SET(CORE "arduino")
ENDIF(DEFINED COMPONENTS_SIZE AND COMPONENTS_SIZE GREATER 0)

# Get the variant name
IF(DEFINED COMPONENTS_SIZE AND COMPONENTS_SIZE GREATER 0)
  LIST(GET Arduino_FIND_COMPONENTS 1 VARIANT)
ELSE(DEFINED COMPONENTS_SIZE AND COMPONENTS_SIZE GREATER 0)
  SET(VARIANT "standard")
ENDIF(DEFINED COMPONENTS_SIZE AND COMPONENTS_SIZE GREATER 0)

# Define COREDIR - defaults to CORE, except on stm32
IF(CORE STREQUAL "stm32")
  SET(COREDIR "arduino")
ELSE(CORE STREQUAL "stm32")
  SET(COREDIR ${CORE})
ENDIF(CORE STREQUAL "stm32")

# Try downloading the official package
IF(CORE STREQUAL "arduino")
  IF(NOT DEFINED Arduino_FIND_VERSION)
    SET(Arduino_FIND_VERSION "1.6.13")
  ENDIF(NOT DEFINED Arduino_FIND_VERSION)
  FIND_OR_DOWNLOAD("arduino-${Arduino_FIND_VERSION}" "https://www.arduino.cc/download.php?f=/arduino-${Arduino_FIND_VERSION}-linux64.tar.xz" "arduino-${Arduino_FIND_VERSION}.tar.xz")
ENDIF(CORE STREQUAL "arduino")
IF(CORE STREQUAL "tivac")
  IF(NOT DEFINED Arduino_FIND_VERSION)
    SET(Arduino_FIND_VERSION "1.0.3")
  ENDIF(NOT DEFINED Arduino_FIND_VERSION)
  FIND_OR_DOWNLOAD("tivac-core-${Arduino_FIND_VERSION}" "https://github.com/energia/tivac-core/archive/${Arduino_FIND_VERSION}.zip" "tivac-core-${Arduino_FIND_VERSION}.zip")
ENDIF(CORE STREQUAL "tivac")
IF(CORE STREQUAL "stm32")
  IF(NOT DEFINED Arduino_FIND_VERSION)
    SET(Arduino_FIND_VERSION "1.2.0")
  ENDIF(NOT DEFINED Arduino_FIND_VERSION)
  FIND_OR_DOWNLOAD("Arduino_Core_STM32-${Arduino_FIND_VERSION}" "https://github.com/stm32duino/Arduino_Core_STM32/archive/${Arduino_FIND_VERSION}.zip" "stm32duino-${Arduino_FIND_VERSION}.zip")
ENDIF(CORE STREQUAL "stm32")
IF(CORE STREQUAL "msp430")
  IF(NOT DEFINED Arduino_FIND_VERSION)
    SET(Arduino_FIND_VERSION "1.0.2")
  ENDIF(NOT DEFINED Arduino_FIND_VERSION)
  FIND_OR_DOWNLOAD("msp430-lg-core-${Arduino_FIND_VERSION}" "https://github.com/energia/msp430-lg-core/archive/${Arduino_FIND_VERSION}.zip" "msp430-lg-core-${Arduino_FIND_VERSION}.zip")
ENDIF(CORE STREQUAL "msp430")

# Find the highest or exact version
FILE(GLOB_RECURSE FOUND "${TOOLCHAIN_ROOT}/*/boards.txt")
FOREACH(Arduino_CANDIDATE ${FOUND})
  GET_FILENAME_COMPONENT(ROOT_DIR ${Arduino_CANDIDATE} PATH)
  IF(EXISTS "${ROOT_DIR}/platform.txt")
    IF(NOT EXISTS "${ROOT_DIR}/cores/${COREDIR}")
      CONTINUE()
    ENDIF(NOT EXISTS "${ROOT_DIR}/cores/${COREDIR}")

    IF(NOT EXISTS "${ROOT_DIR}/variants/${VARIANT}")
      CONTINUE()
    ENDIF(NOT EXISTS "${ROOT_DIR}/variants/${VARIANT}")

    FILE(READ "${ROOT_DIR}/platform.txt" Arduino_README OFFSET 0 LIMIT 512)
    STRING(REGEX REPLACE ".*version=([0-9]+.[0-9]+(.[0-9]+)?).*" "\\1" Arduino_CANDIDATE_VERSION ${Arduino_README})
    IF(NOT DEFINED Arduino_CANDIDATE_VERSION)
      CONTINUE()
    ENDIF(NOT DEFINED Arduino_CANDIDATE_VERSION)
    IF(Arduino_FIND_VERSION_EXACT AND Arduino_CANDIDATE_VERSION VERSION_EQUAL Arduino_FIND_VERSION)
      SET(Arduino_LAST_ROOT_DIR ${ROOT_DIR})
      SET(Arduino_LAST_VERSION ${Arduino_CANDIDATE_VERSION})
      BREAK()
    ELSEIF(Arduino_CANDIDATE_VERSION VERSION_GREATER Arduino_LAST_VERSION)
      SET(Arduino_LAST_ROOT_DIR ${ROOT_DIR})
      SET(Arduino_LAST_VERSION ${Arduino_CANDIDATE_VERSION})
    ENDIF(Arduino_FIND_VERSION_EXACT AND Arduino_CANDIDATE_VERSION VERSION_EQUAL Arduino_FIND_VERSION)
  ENDIF(EXISTS "${ROOT_DIR}/platform.txt")
ENDFOREACH(Arduino_CANDIDATE)

IF(Arduino_LAST_ROOT_DIR)
  IF(Arduino_LAST_VERSION VERSION_LESS Arduino_FIND_VERSION)
    IF(NOT Arduino_FIND_QUIETLY)
      MESSAGE("Arduino version ${Arduino_LAST_VERSION} too old - ${Arduino_FIND_VERSION} is required.")
    ENDIF(NOT Arduino_FIND_QUIETLY)
  ELSE(Arduino_LAST_VERSION VERSION_LESS Arduino_FIND_VERSION)
    SET(Arduino_VERSION ${Arduino_LAST_VERSION} CACHE INTERNAL "Arduino version")
    IF(Arduino_VERSION VERSION_EQUAL Arduino_FIND_VERSION)
      SET(Arduino_VERSION_EXACT 1 CACHE INTERNAL "Arduino version is exact")
    ENDIF(Arduino_VERSION VERSION_EQUAL Arduino_FIND_VERSION)
    
    SET(Arduino_FOUND 1 CACHE INTERNAL "Arduino found")
    SET(Arduino_INCLUDE_DIRS "${Arduino_LAST_ROOT_DIR}/cores/${COREDIR}" "${Arduino_LAST_ROOT_DIR}/variants/${VARIANT}" "${Arduino_LAST_ROOT_DIR}/system" CACHE PATH "Arduino include path")
    SET(Arduino_LIBRARY_DIR "${Arduino_LAST_ROOT_DIR}/libraries" CACHE PATH "Arduino core libraries")

    IF(NOT Arduino_FIND_QUIETLY)
      MESSAGE("Found Arduino (${CORE}:${VARIANT}) version ${Arduino_VERSION}")
    ENDIF(NOT Arduino_FIND_QUIETLY)

    # Add all sources under it to Arduino_SOURCES
    FILE(GLOB_RECURSE Arduino_SOURCES "${Arduino_LAST_ROOT_DIR}/cores/${COREDIR}/*.[cCsS]"
            "${Arduino_LAST_ROOT_DIR}/cores/${COREDIR}/*.[cC][pP][pP]"
            "${Arduino_LAST_ROOT_DIR}/cores/${COREDIR}/**/*.[cC][pP][pP]"
	    "${Arduino_LAST_ROOT_DIR}/system/**/*.[cCsS]"
	    "${Arduino_LAST_ROOT_DIR}/variants/${VARIANT}/*.[cCsS]"
	    "${Arduino_LAST_ROOT_DIR}/variants/${VARIANT}/*.[cC][pP][pP]")

    IF(CORE STREQUAL "tivac")
	    SET(Arduino_COMPILE_FLAGS -Dgcc -DARDUINO_ARCH_AVR -DARDUINO=10610)
    ENDIF(CORE STREQUAL "tivac")

    IF(CORE STREQUAL "tivac")
      FILE(GLOB_RECURSE Arduino_LINKER_SCRIPT "${Arduino_LAST_ROOT_DIR}/cores/${COREDIR}/*blizzard.ld")
      # Remove EPI workaround file form sources, it is not needed
      LIST(REMOVE_ITEM Arduino_SOURCES "${Arduino_LAST_ROOT_DIR}/system/driverlib/epi_workaround_ccs.s")
      SET(Arduino_COMPILE_FLAGS $<$<COMPILE_LANGUAGE:ASM>:-Xassembler -mccs> -Dgcc -DENERGIA_ARCH_TIVAC -DENERGIA_EK_TM4C123GXL -DARDUINO=10610 -DENERGIA=10002)
    ENDIF(CORE STREQUAL "tivac")

    IF(CORE STREQUAL "msp430")
      SET(Arduino_COMPILE_FLAGS -mmcu=${CPU} -Dgcc -DENERGIA_ARCH_MSP430 -DARDUINO=10610 -DENERGIA=10002)
    ENDIF(CORE STREQUAL "msp430")

    IF(CORE STREQUAL "arduino")
      SET(Arduino_COMPILE_FLAGS -mmcu=${CPU} -Dgcc -DARDUINO=10610)
    ENDIF(CORE STREQUAL "arduino")

    IF(CORE STREQUAL "stm32")
      FILE(GLOB_RECURSE Arduino_LINKER_SCRIPT "${Arduino_LAST_ROOT_DIR}/variants/${VARIANT}/ldscript.ld")
      SET(Arduino_COMPILE_FLAGS -Dgcc -DARDUINO_ARCH_AVR -DARDUINO=10610)
    ENDIF(CORE STREQUAL "stm32")

    # Prepare the library
    ADD_LIBRARY(Arduino STATIC ${Arduino_SOURCES})
    TARGET_INCLUDE_DIRECTORIES(Arduino PRIVATE ${Arduino_INCLUDE_DIRS})
    TARGET_COMPILE_OPTIONS(Arduino PRIVATE ${Arduino_COMPILE_FLAGS} -DF_CPU=${F_CPU})
    SET_PROPERTY(TARGET Arduino APPEND_STRING PROPERTY LINK_FLAGS " -Wl,-Map=_arduino.map")
    SET(Arduino_LIBRARY Arduino)
    IF(CORE STREQUAL "tivac")
      SET(Arduino_LIBRARIES -Wl,--entry=ResetISR ${Arduino_LIBRARY})
    ELSEIF(CORE STREQUAL "stm32")
      SET(Arduino_LIBRARIES -Wl,--entry=Reset_Handler ${Arduino_LIBRARY})
    ELSE()
      SET(Arduino_LIBRARIES -Wl,-u,main ${Arduino_LIBRARY})
    ENDIF(CORE STREQUAL "tivac")

    IF(CORE STREQUAL "msp430")
      TARGET_INCLUDE_DIRECTORIES(Arduino PRIVATE ${TOOLCHAIN_INCLUDE_DIRS})
    ENDIF(CORE STREQUAL "msp430")

  ENDIF(Arduino_LAST_VERSION VERSION_LESS Arduino_FIND_VERSION)

ELSE(Arduino_LAST_ROOT_DIR)
  IF(NOT Arduino_FIND_QUIETLY)
    IF(Arduino_FIND_REQUIRED)
      MESSAGE("Arduino (${CORE}:${VARIANT}) not found")
    ELSE(Arduino_FIND_REQUIRED)
      MESSAGE(WARNING "Arduino (${CORE}:${VARIANT}) not found")
    ENDIF(Arduino_FIND_REQUIRED)
  ENDIF(NOT Arduino_FIND_QUIETLY)
ENDIF(Arduino_LAST_ROOT_DIR)

