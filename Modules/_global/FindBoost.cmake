# - Try to find Boost, but do not compile! Use as header only lib
# Once done this will define
#  Boost_FOUND - There is Boost in the library path
#  Boost_INCLUDE_DIRS - Boost include directories

IF(NOT DEFINED Boost_FIND_VERSION)
  SET(Boost_FIND_VERSION "1.66.0")
ENDIF(NOT DEFINED Boost_FIND_VERSION)

STRING(REPLACE "." "_" Boost_VERSION_US ${Boost_FIND_VERSION})

FIND_OR_DOWNLOAD("boost_${Boost_VERSION_US}" "https://dl.bintray.com/boostorg/release/${Boost_FIND_VERSION}/source/boost_${Boost_VERSION_US}.tar.gz" "boost-${Boost_VERSION_US}.zip")

# Find the highest or exact version
FILE(GLOB FOUND "${TOOLCHAIN_ROOT}/*/index.html")
FOREACH(Boost_CANDIDATE ${FOUND})
  GET_FILENAME_COMPONENT(ROOT_DIR ${Boost_CANDIDATE} PATH)
  FILE(READ "${ROOT_DIR}/index.html" Boost_README OFFSET 0 LIMIT 4096)
  STRING(REGEX REPLACE ".*http://www.boost.org/users/history/version_([0-9]+_[0-9]+_[0-9]+(_(beta[0-9]+))?).html.*" "\\1" Boost_CANDIDATE_VERSION_US ${Boost_README})
  STRING(REPLACE "_" "." Boost_CANDIDATE_VERSION ${Boost_CANDIDATE_VERSION_US})

  IF(NOT DEFINED Boost_CANDIDATE_VERSION)
    CONTINUE()
  ENDIF(NOT DEFINED Boost_CANDIDATE_VERSION)
  IF(Boost_FIND_VERSION_EXACT AND Boost_CANDIDATE_VERSION VERSION_EQUAL Boost_FIND_VERSION)
    SET(Boost_LAST_ROOT_DIR ${ROOT_DIR})
    SET(Boost_LAST_VERSION ${Boost_CANDIDATE_VERSION})
    BREAK()
  ELSEIF(Boost_CANDIDATE_VERSION VERSION_GREATER Boost_LAST_VERSION)
    SET(Boost_LAST_ROOT_DIR ${ROOT_DIR})
    SET(Boost_LAST_VERSION ${Boost_CANDIDATE_VERSION})
  ENDIF(Boost_FIND_VERSION_EXACT AND Boost_CANDIDATE_VERSION VERSION_EQUAL Boost_FIND_VERSION)
ENDFOREACH(Boost_CANDIDATE)

IF(Boost_LAST_ROOT_DIR)
  IF(Boost_LAST_VERSION VERSION_LESS Boost_FIND_VERSION)
    IF(NOT Boost_FIND_QUIETLY)
      MESSAGE("Boost version ${Boost_LAST_VERSION} too old - ${Boost_FIND_VERSION} is required.")
    ENDIF(NOT Boost_FIND_QUIETLY)
  ELSE(Boost_LAST_VERSION VERSION_LESS Boost_FIND_VERSION)
    SET(Boost_VERSION ${Boost_LAST_VERSION} CACHE INTERNAL "Boost version")
    IF(Boost_VERSION VERSION_EQUAL Boost_FIND_VERSION)
      SET(Boost_VERSION_EXACT 1 CACHE INTERNAL "Boost version is exact")
    ENDIF(Boost_VERSION VERSION_EQUAL Boost_FIND_VERSION)
    
    SET(Boost_FOUND 1 CACHE INTERNAL "Boost found")
    SET(Boost_INCLUDE_DIRS "${Boost_LAST_ROOT_DIR}" CACHE PATH "Boost include path")

    IF(NOT Boost_FIND_QUIETLY)
      MESSAGE("Found Boost version ${Boost_VERSION}")
    ENDIF(NOT Boost_FIND_QUIETLY)


  ENDIF(Boost_LAST_VERSION VERSION_LESS Boost_FIND_VERSION)

ELSE(Boost_LAST_ROOT_DIR)
  IF(NOT Boost_FIND_QUIETLY)
    IF(Boost_FIND_REQUIRED)
      MESSAGE("Boost not found")
    ELSE(Boost_FIND_REQUIRED)
      MESSAGE(WARNING "Boost not found")
    ENDIF(Boost_FIND_REQUIRED)
  ENDIF(NOT Boost_FIND_QUIETLY)
ENDIF(Boost_LAST_ROOT_DIR)

