# - Try to find OpenOCD tool and script directory
# Once done this will define
#  OpenOCD_FOUND - There is OpenOCD available
#  OpenOCD - OpenOCD binary
#  OpenOCD_PREFIX - The prefix root of OpenOCD
#  OpenOCD_SCRIPT_DIR - The root of builtin OpenOCD scripts
#  OpenOCD_VERSION - OpenOCD version string

FIND_PROGRAM(OpenOCD "openocd" DOC "OpenOCD binary")

IF(OpenOCD)
  # Get OpenOCD version
  EXECUTE_PROCESS(COMMAND ${OpenOCD} --version ERROR_VARIABLE OpenOCD_VERSION_OUT OUTPUT_QUIET)
  STRING(REGEX REPLACE "Open On-Chip Debugger ([0-9]+.[0-9]+(.[0-9]+)?).*" "\\1" OpenOCD_VERSION ${OpenOCD_VERSION_OUT})

  IF(OpenOCD_VERSION VERSION_LESS OpenOCD_FIND_VERSION)
    IF(NOT OpenOCD_FIND_QUIETLY)
      MESSAGE("OpenOCD version ${OpenOCD_VERSION} too old - ${OpenOCD_FIND_VERSION} is required.")
    ENDIF(NOT OpenOCD_FIND_QUIETLY)
  ELSE(OpenOCD_VERSION VERSION_LESS OpenOCD_FIND_VERSION)
    SET(OpenOCD_VERSION ${OpenOCD_VERSION} CACHE INTERNAL "OpenOCD version")
    SET(OpenOCD_FOUND 1 CACHE INTERNAL "OpenOCD found")

    IF(NOT OpenOCD_FIND_QUIETLY)
      MESSAGE("found OpenOCD version ${OpenOCD_VERSION}")
    ENDIF(NOT OpenOCD_FIND_QUIETLY)

    GET_FILENAME_COMPONENT(OpenOCD_BIN_DIR ${OpenOCD} PATH)
    GET_FILENAME_COMPONENT(OpenOCD_PREFIX ${OpenOCD_BIN_DIR} PATH)
    SET(OpenOCD_PREFIX ${OpenOCD_PREFIX} CACHE PATH "OpenOCD prefix path")
    SET(OpenOCD_SCRIPT_DIR "${OpenOCD_PREFIX}/share/openocd/scripts" CACHE PATH "OpenOCD scripts directory")
  ENDIF(OpenOCD_VERSION VERSION_LESS OpenOCD_FIND_VERSION)
ELSE(OpenOCD)
  IF(NOT OpenOCD_FIND_QUIETLY)
    IF(OpenOCD_FIND_REQUIRED)
      MESSAGE("OpenOCD not found")
    ELSE(OpenOCD_FIND_REQUIRED)
      MESSAGE(WARNING "OpenOCD not found")
    ENDIF(OpenOCD_FIND_REQUIRED)
  ENDIF(NOT OpenOCD_FIND_QUIETLY)
ENDIF(OpenOCD)


