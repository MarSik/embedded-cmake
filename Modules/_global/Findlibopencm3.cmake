# - Try to find libopencm3
# Once done this will define
#  libopencm3_FOUND - There is libopencm3 in the library path
#  libopencm3_INCLUDE_DIRS - libopencm3 include directories
#  libopencm3_SOURCES
#  libopencm3_LIBRARY_DIR - libopencm3 internal libraries

#  libopencm3_FIND_COMPONENTS = [vendor family]

#  Compute paths
JOIN("${libopencm3_FIND_COMPONENTS}" "/" libopencm3_DEVICE_PATH)
JOIN("${libopencm3_FIND_COMPONENTS}" "" libopencm3_DEVICE_FAMILY)
STRING(TOUPPER ${libopencm3_DEVICE_FAMILY} libopencm3_DEVICE_FAMILY_SYMBOL)

# Try downloading the official package
FIND_OR_DOWNLOAD("libopencm3-master" "https://github.com/libopencm3/libopencm3/archive/master.zip" "libopencm3-master.zip")

# Build opencm3
MESSAGE("Make sure libopencm3 was already built manually")

SET(libopencm3_LAST_ROOT_DIR "${TOOLCHAIN_ROOT}/libopencm3-master")
SET(libopencm3_VERSION "0.0.0-0.master" CACHE INTERNAL "libopencm3 version")
SET(libopencm3_ROOT "${libopencm3_LAST_ROOT_DIR}" CACHE INTERNAL "libopencm3 root dir")
SET(libopencm3_FOUND 1 CACHE INTERNAL "libopencm3 found")
SET(libopencm3_INCLUDE_DIRS "${libopencm3_LAST_ROOT_DIR}/include" CACHE PATH "libopencm3 include path")
SET(libopencm3_COMPILE_FLAGS "-D${libopencm3_DEVICE_FAMILY_SYMBOL}" CACHE PATH "libopencm3 compile flags")

IF(NOT libopencm3_FIND_QUIETLY)
MESSAGE("Found libopencm3")
ENDIF(NOT libopencm3_FIND_QUIETLY)

# FILE(GLOB_RECURSE libopencm3_LINKER_SCRIPT "${libopencm3_LAST_ROOT_DIR}/cores/${CORE}/*blizzard.ld")

SET(libopencm3_LIBRARIES -L"${libopencm3_LAST_ROOT_DIR}/lib" -Wl,--entry=reset_handler "${libopencm3_LAST_ROOT_DIR}/lib/libopencm3_${libopencm3_DEVICE_FAMILY}.a")

