# - Try to find StellarisWare (SW-LM4F-DRL)
# Once done this will define
#  StellarisWare_FOUND - There is the StellarisWare StdPeriph Lib in the library path
#  StellarisWare_INCLUDE_DIRS - include directories
#  StellarisWare_SOURCES - source files to include during compilation

FIND_PATH_RECURSIVE(TW_H_DIR "lm4f120h5qr.h" ${TOOLCHAIN_ROOT})

IF(TW_H_DIR)
SET(StellarisWare_FOUND 1 CACHE INTERNAL "Stellaris StdPeriph library found")
SET(StellarisWare_INCLUDE_DIRS "${TW_H_DIR}/../" CACHE PATH "Stellaris Chip + StdPeriph library include root")

# Find all device driver sources
FILE(GLOB TW_DRIVER_SOURCES "${TW_H_DIR}/../driverlib/*.c")
SET(StellarisWare_SOURCES ${TW_DRIVER_SOURCES} CACHE PATH "Stellaris StdPeriph source files")

ENDIF(TW_H_DIR)

