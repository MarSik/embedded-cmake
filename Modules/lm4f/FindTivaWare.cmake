# - Try to find TivaWare (SW-TM4C-DRL)
# Once done this will define
#  TivaWare_FOUND - There is the TivaWare StdPeriph Lib in the library path
#  TivaWare_INCLUDE_DIRS - include directories
#  TivaWare_SOURCES - source files to include during compilation

FIND_PATH_RECURSIVE(TW_H_DIR "tm4c1231h6pge.h" ${TOOLCHAIN_ROOT})

IF(TW_H_DIR)
SET(TivaWare_FOUND 1 CACHE INTERNAL "Tiva StdPeriph library found")
SET(TivaWare_INCLUDE_DIRS "${TW_H_DIR}/../" CACHE PATH "Tiva Chip + StdPeriph library include root")

# Find all device driver sources
FILE(GLOB TW_DRIVER_SOURCES "${TW_H_DIR}/../driverlib/*.c")
SET(TivaWare_SOURCES ${TW_DRIVER_SOURCES} CACHE PATH "Tiva StdPeriph source files")

ENDIF(TW_H_DIR)

