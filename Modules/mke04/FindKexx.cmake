# - Try to find Kinetis Driverlib
# http://www.nxp.com/webapp/sps/download/license.jsp?colCode=KEXX_DRIVERS_V1.2.1_DEVD&Parent_nodeId=1394134641432714881463&Parent_pageType=product&Parent_nodeId=1394134641432714881463&Parent_pageType=product
# Once done this will define
#  Kexx_FOUND - There is the Kinetis driverlib in the library path
#  Kexx_INCLUDE_DIRS - include directories
#  Kexx_SOURCES - source files to include during compilation

FIND_PATH_RECURSIVE(Kexx_H_DIR "MKE04Z4.h" ${TOOLCHAIN_ROOT})

IF(Kexx_H_DIR)
SET(Kexx_FOUND 1)
SET(Kexx_INCLUDE_DIRS "${Kexx_H_DIR}" "${Kexx_H_DIR}/../" "${Kexx_H_DIR}/../../common/" "${Kexx_H_DIR}/../../drivers/")

# Find all device driver sources
FILE(GLOB Kexx_DRIVER_SOURCES "${Kexx_H_DIR}/../../drivers/*/*.c")
SET(Kexx_SOURCES ${Kexx_DRIVER_SOURCES})

# Prepare the library
ADD_LIBRARY(Kexx STATIC ${Kexx_SOURCES})
TARGET_INCLUDE_DIRECTORIES(Kexx PRIVATE ${Kexx_INCLUDE_DIRS})
TARGET_COMPILE_OPTIONS(Kexx PRIVATE -DF_CPU=${F_CPU})
SET_PROPERTY(TARGET Kexx APPEND_STRING PROPERTY LINK_FLAGS " -Wl,-Map=_kexx.map")
SET(Kexx_LIBRARY Kexx)
SET(Kexx_LIBRARIES ${Kexx_LIBRARY})

ENDIF(Kexx_H_DIR)

