# - Try to find STM32F30X header definitions
# Once done this will define
#  STM32F30X_FOUND - There is the STM32F30X toolchain in the library path
#  STM32F30X_INCLUDE_DIRS - include directories
#  STM32F30X_SOURCES - source files to include during compilation

FIND_PATH_RECURSIVE(STM32F30X_H_DIR "stm32f30x.h" ${TOOLCHAIN_ROOT})

IF(STM32F30X_H_DIR)
SET(STM32F30X_FOUND 1 CACHE INTERNAL "STM32F30X definitions found")
SET(STM32F30X_INCLUDE_DIRS ${STM32F30X_H_DIR} CACHE PATH "STM32F30X include directory")
ENDIF(STM32F30X_H_DIR)

