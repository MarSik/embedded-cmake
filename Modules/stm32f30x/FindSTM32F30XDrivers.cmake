# - Try to find STM32F30XDrivers
# Once done this will define
#  STM32F30XDrivers_FOUND - There is the STM32F30X StdPeriphLib in the library path
#  STM32F30XDrivers_INCLUDE_DIRS - include directories
#  STM32F30XDrivers_SOURCES - source files to include during compilation

FIND_PATH_RECURSIVE(STM32F30X_DRIVER_H_DIR "stm32f30x_rcc.h" ${TOOLCHAIN_ROOT})

IF(STM32F30X_DRIVER_H_DIR)
SET(STM32F30XDrivers_FOUND 1 CACHE INTERNAL "STM32F30X StdPeriph library")
SET(STM32F30XDrivers_INCLUDE_DIRS ${STM32F30X_DRIVER_H_DIR} CACHE PATH "STM32F30X StdPeriph library include directory")

# Find all device driver sources
FILE(GLOB STM32F30X_DRIVER_SOURCES "${STM32F30X_DRIVER_H_DIR}/../src/*.c")
SET(STM32F30XDrivers_SOURCES ${STM32F30X_DRIVER_SOURCES} CACHE PATH "STM32F30X StdPeriph source files")

ENDIF(STM32F30X_DRIVER_H_DIR)


