# - Try to find STM32L1 header definitions
# Once done this will define
#  STM32L1_FOUND - There is the STM32L1 toolchain in the library path
#  STM32L1_INCLUDE_DIRS - include directories
#  STM32L1_SOURCES - source files to include during compilation

FIND_PATH_RECURSIVE(STM32L1_H_DIR "stm32l1xx.h" ${TOOLCHAIN_ROOT})

IF(STM32L1_H_DIR)
SET(STM32L1_FOUND 1 CACHE INTERNAL "STM32L1 definitions found")
SET(STM32L1_INCLUDE_DIRS ${STM32L1_H_DIR} CACHE PATH "STM32L1 include directory")
ENDIF(STM32L1_H_DIR)

