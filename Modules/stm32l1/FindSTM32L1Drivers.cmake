# - Try to find STM32L1Drivers
# Once done this will define
#  STM32L1Drivers_FOUND - There is the STM32L1 StdPeriphLib in the library path
#  STM32L1Drivers_INCLUDE_DIRS - include directories
#  STM32L1Drivers_SOURCES - source files to include during compilation

FIND_PATH_RECURSIVE(STM32L1_DRIVER_H_DIR "stm32l1xx_rcc.h" ${TOOLCHAIN_ROOT})

IF(STM32L1_DRIVER_H_DIR)
SET(STM32L1Drivers_FOUND 1 CACHE INTERNAL "STM32L1 StdPeriph library")
SET(STM32L1Drivers_INCLUDE_DIRS ${STM32L1_DRIVER_H_DIR} CACHE PATH "STM32L1 StdPeriph library include directory")

# Find all device driver sources
FILE(GLOB STM32L1_DRIVER_SOURCES "${STM32L1_DRIVER_H_DIR}/../src/*.c")
SET(STM32L1Drivers_SOURCES ${STM32L1_DRIVER_SOURCES} CACHE PATH "STM32L1 StdPeriph source files")

ENDIF(STM32L1_DRIVER_H_DIR)


