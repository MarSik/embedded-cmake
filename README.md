# CMake toolchain and include files for embedded development using GCC

## Supported toolchains and CPU families

 - arm-none-eabi-gcc
   - STM32 CPUs (with Std Peripheral library)
   - EFM32 CPUs (with GeckoSDK)
 - AVR 8bit CPUs (using avr-gcc)
 - MSP430 CPUs (using msp430-gcc)

## SDKs

The SDKs are searched for in `${TOOLCHAIN_ROOT}` that is set to `$HOME/Developer` by the default value in `paths.cmake`.

## Usage

Check the example CMakeFiles.txt in the `Examples` directory. The important assumptions are:

 - the main target has the same name as the project
 - the CPU type is set to the CPU variable
 - `${TOOLCHAIN_INIT}` is included after the main target is defined 

## Build

```
mkdir build
pushd build
cmake -DMAKE_BUILD_TYPE=<Debug|Release> -DCMAKE_TOOLCHAIN_FILE=<Toolchains/cpu.cmake>
make
```

