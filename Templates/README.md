# CMakefile templates

This directory contains a subdirectory for each platform.

Each platform subdirectory contains Jinja2 templates that can be used
to prepare a new project's CMakefile and other files.

The dirnames must match the Toolchains. All files within those are
going to be copied to the project after Jinja2 treatment.

