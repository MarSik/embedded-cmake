#ifndef KE04_H_OK7MS
#define KE04_H_OK7MS

#include <stdint.h>

#define IRQn_Type uint8_t
#define SysTick_IRQn ((IRQn_Type)15)
#define __NVIC_PRIO_BITS 2

#include "core_cm0plus.h"



#endif
