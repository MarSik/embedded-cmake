= Toolchain files

The files in this directory are the entrypoints for project configuration.
They should form a hierarchy and inherit from each other.

Eg.: efm32gw - efm32 - cortex-m4 - arm-none-eabi - embedded

The toolchain files should ONLY:

- download and define toolchain (gcc..)
- define compilation flags that are necessary for any project
  using that toolchain and cpu


The toolchain file name used for configuring a project is then projected
to TOOLCHAIN\_INIT variable that points to a file from Includes/ directory
and defines all the nice-to-have functions and macros for the cpu and toolchain
based projects.

