INCLUDE("${CMAKE_CURRENT_LIST_DIR}/../paths.cmake")

TOOLCHAIN_FILE_START()

SET(CMAKE_SYSTEM_NAME Generic)
SET(CMAKE_SYSTEM_PROCESSOR avr)
SET(TOOL_PREFIX avr-)

SET(AVR_C_FLAGS "${AVR_C_FLAGS} -fpack-struct -fshort-enums -funsigned-char -funsigned-bitfields -ffunction-sections -fdata-sections -fomit-frame-pointer -fno-unroll-loops -ffast-math" CACHE STRING "AVR related C flags")

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${AVR_CPU_FLAGS} -Wno-main -Wall -Wstrict-prototypes -std=gnu99 ${AVR_C_FLAGS}" CACHE STRING "c compiler flags")
SET(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -fno-builtin -O0 -gdwarf-2 -g3" CACHE STRING "c compiler flags debug" FORCE)
SET(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -Os" CACHE STRING "c compiler flags release" FORCE)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${AVR_CPU_FLAGS} -Wno-main -Wall -std=gnu11 -fno-exceptions -fno-asynchronous-unwind-tables -fno-use-cxa-atexit ${AVR_C_FLAGS}" CACHE STRING "cxx compiler flags")
SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fno-builtin -O0 -gdwarf-2 -g3" CACHE STRING "cxx compiler flags debug")
SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -finline-small-functions -findirect-inlining -Os" CACHE STRING "cxx compiler flags release")

SET(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} ${AVR_CPU_FLAGS} -x assembler-with-cpp" CACHE STRING "asm compiler flags")
SET(CMAKE_ASM_FLAGS_DEBUG "${CMAKE_ASM_FLAGS_DEBUG} -O0 -gdwarf-2 -g3" CACHE STRING "asm compiler flags debug")
SET(CMAKE_ASM_FLAGS_RELEASE "${CMAKE_ASM_FLAGS_RELEASE} -Os" CACHE STRING "asm compiler flags release")

SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static ${AVR_CPU_FLAGS}" CACHE STRING "executable linker flags")
SET(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG}" CACHE STRING "linker flags debug")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE}" CACHE STRING "linker flags release")

SET(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -static ${AVR_CPU_FLAGS}" CACHE STRING "module linker flags")

INCLUDE("${CMAKE_CURRENT_LIST_DIR}/embedded.include")

