INCLUDE("${CMAKE_CURRENT_LIST_DIR}/../paths.cmake")

TOOLCHAIN_FILE_START()

# Find toolchain pieces
FIND_OR_DOWNLOAD("gcc-arm-none-eabi-6-2017-q2-update" "https://developer.arm.com/-/media/Files/downloads/gnu-rm/6-2017q2/gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2?revision=2cc92fb5-3e0e-402d-9197-bdfc8224d8a5?product=GNU%20Arm%20Embedded%20Toolchain,64-bit,,Linux,6-2017-q2-update" "gcc-arm-none-eabi-6-2017-q2-update.tar.bz2")
SET(COMPILER_PATH_HINT "${TOOLCHAIN_ROOT}/gcc-arm-none-eabi-6-2017-q2-update/bin")
SET(LINKER_PATH_HINT "${TOOLCHAIN_ROOT}/gcc-arm-none-eabi-6-2017-q2-update/bin")
SET(BINUTILS_PATH_HINT "${TOOLCHAIN_ROOT}/gcc-arm-none-eabi-6-2017-q2-update/bin")

SET(CMAKE_EXE_LINKER_FLAGS "-nostartfiles -specs=nano.specs -specs=nosys.specs")
SET(ARM_CPU_FLAGS "${ARM_CPU_FLAGS} -D__FPU_PRESENT=1")

# Include generic chip family cmake script
INCLUDE("${CMAKE_CURRENT_LIST_DIR}/cortex-m4.include")

