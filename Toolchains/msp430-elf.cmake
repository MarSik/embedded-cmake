INCLUDE("${CMAKE_CURRENT_LIST_DIR}/../paths.cmake")

TOOLCHAIN_FILE_START()

SET(TOOL_PREFIX msp430-elf- CACHE string "Compiler and tool executable prefix")

# Disable compiler checks as those do not know about the selected CPU
# during the toolchain file processing and will fail
set(CMAKE_C_COMPILER_WORKS 0 CACHE bool "Override for C compiler check")
set(CMAKE_C_ABI_COMPILED 0 CACHE bool "Override for C compiler ABI check")
set(CMAKE_CXX_COMPILER_WORKS 0 CACHE bool "Override for C++ compiler check")
set(CMAKE_ASM_COMPILER_WORKS 0 CACHE bool "Override for ASM compiler check")

INCLUDE("${CMAKE_CURRENT_LIST_DIR}/msp430.cmake")

