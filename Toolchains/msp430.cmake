INCLUDE("${CMAKE_CURRENT_LIST_DIR}/../paths.cmake")

TOOLCHAIN_FILE_START()

# Find toolchain pieces
FIND_OR_DOWNLOAD("msp430-gcc-6.4.0.32_linux64" "http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/exports/msp430-gcc-6.4.0.32_linux64.tar.bz2" "msp430-gcc-6.4.0.32_linux64.tar.bz2")
SET(COMPILER_PATH_HINT "${TOOLCHAIN_ROOT}/msp430-gcc-6.4.0.32_linux64/bin")
SET(LINKER_PATH_HINT "${TOOLCHAIN_ROOT}/msp430-gcc-6.4.0.32_linux64/bin")
SET(BINUTILS_PATH_HINT "${TOOLCHAIN_ROOT}/msp430-gcc-6.4.0.32_linux64/bin")

# Download toolchain support files
FIND_OR_DOWNLOAD("msp430-gcc-support-files" "http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/exports/msp430-gcc-support-files-1.203.zip" "msp430-gcc-support-files-1.203.zip")
SET(TOOLCHAIN_INCLUDE_DIRS "${TOOLCHAIN_ROOT}/msp430-gcc-support-files/include")

FIND_PROGRAM(MSPDEBUG "mspdebug" DOC "mspdebug tool")

SET(CMAKE_SYSTEM_NAME Generic)
SET(CMAKE_SYSTEM_PROCESSOR msp430)
IF(NOT DEFINED TOOL_PREFIX)
SET(TOOL_PREFIX msp430-elf- CACHE string "Compiler and tool executable prefix")
ENDIF(NOT DEFINED TOOL_PREFIX)

# Disable compiler checks as those do not know about the selected CPU
# during the toolchain file processing and will fail
set(CMAKE_C_COMPILER_WORKS 1 CACHE bool "Override for C compiler check")
set(CMAKE_C_ABI_COMPILED 1 CACHE bool "Override for C compiler ABI check")
set(CMAKE_CXX_COMPILER_WORKS 1 CACHE bool "Override for C++ compiler check")
set(CMAKE_ASM_COMPILER_WORKS 1 CACHE bool "Override for ASM compiler check")

SET(CHIP_C_FLAGS "${CHIP_C_FLAGS} -fpack-struct -fshort-enums -funsigned-char -funsigned-bitfields -fno-exceptions -ffunction-sections -fdata-sections -fomit-frame-pointer -fno-unroll-loops -ffast-math -ftree-vectorize" CACHE STRING "chip family related C flags")

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${CHIP_CPU_FLAGS} -I${TOOLCHAIN_INCLUDE_DIRS} -Wno-main -Wall -Wstrict-prototypes -std=gnu99 ${CHIP_C_FLAGS}" CACHE STRING "c compiler flags")
SET(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -fno-builtin -O0 -gdwarf-2 -g3" CACHE STRING "c compiler flags debug" FORCE)
SET(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -Os" CACHE STRING "c compiler flags release" FORCE)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CHIP_CPU_FLAGS} -I${TOOLCHAIN_INCLUDE_DIRS} -Wno-main -Wall -std=gnu++14 -fno-rtti -fno-enforce-eh-specs -fno-exceptions -fno-asynchronous-unwind-tables -fno-use-cxa-atexit ${CHIP_C_FLAGS}" CACHE STRING "cxx compiler flags")
SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fno-builtin -O0 -gdwarf-2 -g3" CACHE STRING "cxx compiler flags debug")
SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -finline-small-functions -findirect-inlining -Os" CACHE STRING "cxx compiler flags release")

SET(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} ${CHIP_CPU_FLAGS} -x assembler-with-cpp" CACHE STRING "asm compiler flags")
SET(CMAKE_ASM_FLAGS_DEBUG "${CMAKE_ASM_FLAGS_DEBUG} -O0 -gdwarf-2 -g3" CACHE STRING "asm compiler flags debug")
SET(CMAKE_ASM_FLAGS_RELEASE "${CMAKE_ASM_FLAGS_RELEASE} -Os" CACHE STRING "asm compiler flags release")

SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -L${TOOLCHAIN_INCLUDE_DIRS} -static -Wl,--gc-sections ${CHIP_CPU_FLAGS}" CACHE STRING "executable linker flags")
SET(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG}" CACHE STRING "linker flags debug")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE}" CACHE STRING "linker flags release")

SET(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -static ${CHIP_CPU_FLAGS}" CACHE STRING "module linker flags")

INCLUDE("${CMAKE_CURRENT_LIST_DIR}/embedded.include")

