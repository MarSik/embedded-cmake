# STM32F0 family
INCLUDE("${CMAKE_CURRENT_LIST_DIR}/../paths.cmake")

TOOLCHAIN_FILE_START()

# Find toolchain pieces
FIND_OR_DOWNLOAD("gcc-arm-none-eabi-6-2017-q1-update" "https://developer.arm.com/-/media/Files/downloads/gnu-rm/6_1-2017q1/gcc-arm-none-eabi-6-2017-q1-update-linux.tar.bz2?product=GNU%20ARM%20Embedded%20Toolchain,64-bit,,Linux,6-2017-q1-update" "gcc-arm-none-eabi-6-2017-q1-update.tar.bz2")
SET(COMPILER_PATH_HINT "${TOOLCHAIN_ROOT}/gcc-arm-none-eabi-6-2017-q1-update/bin")
SET(LINKER_PATH_HINT "${TOOLCHAIN_ROOT}/gcc-arm-none-eabi-6-2017-q1-update/bin")
SET(BINUTILS_PATH_HINT "${TOOLCHAIN_ROOT}/gcc-arm-none-eabi-6-2017-q1-update/bin")

SET(CMAKE_EXE_LINKER_FLAGS "-nostartfiles -specs=nano.specs -specs=nosys.specs")

# Include generic chip family cmake script
INCLUDE("${CMAKE_CURRENT_LIST_DIR}/cortex-m0plus.include")

