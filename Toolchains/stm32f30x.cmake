INCLUDE("${CMAKE_CURRENT_LIST_DIR}/../paths.cmake")

TOOLCHAIN_FILE_START()

# Find toolchain pieces
FIND_PACKAGE(CMSIS 4.0 REQUIRED)
FIND_PACKAGE(STM32F30X REQUIRED)
FIND_PACKAGE(STM32F30XDrivers REQUIRED)

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -nostdlib")
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -nostdlib")
SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -nostdlib")

# List of files to add during compilation
SET(TOOLCHAIN_SOURCES ${CMSIS_SOURCES} ${STM32F30XDrivers_SOURCES} ${STM32F30X_SOURCES} "${CMAKE_CURRENT_LIST_DIR}/../startup/stm32f30x.S" CACHE STRING "Toolchain sources")

# Linker script template for this CPU
SET(LINKER_SCRIPT_TEMPLATE "${CMAKE_CURRENT_LIST_DIR}/../ld/stm32f30x.ld.in" CACHE STRING "Linker script template")

# Include generic chip family cmake script
INCLUDE("${CMAKE_CURRENT_LIST_DIR}/cortex-m4.include")

