# STM32L0 family
INCLUDE("${CMAKE_CURRENT_LIST_DIR}/../paths.cmake")

TOOLCHAIN_FILE_START()

# Find toolchain pieces
FIND_OR_DOWNLOAD("gcc-arm-none-eabi-9-2019-q4-major" "https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/RC2.1/gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2?revision=6e63531f-8cb1-40b9-bbfc-8a57cdfc01b4&la=en&hash=F761343D43A0587E8AC0925B723C04DBFB848339" "gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2")
SET(COMPILER_PATH_HINT "${TOOLCHAIN_ROOT}/gcc-arm-none-eabi-9-2019-q4-major/bin")
SET(LINKER_PATH_HINT "${TOOLCHAIN_ROOT}/gcc-arm-none-eabi-9-2019-q4-major/bin")
SET(BINUTILS_PATH_HINT "${TOOLCHAIN_ROOT}/gcc-arm-none-eabi-9-2019-q4-major/bin")

SET(CMAKE_EXE_LINKER_FLAGS "-nostartfiles -specs=nano.specs -specs=nosys.specs")

# Include generic chip family cmake script
INCLUDE("${CMAKE_CURRENT_LIST_DIR}/cortex-m0plus.include")

