INCLUDE("${CMAKE_CURRENT_LIST_DIR}/../paths.cmake")

TOOLCHAIN_FILE_START()

# Find toolchain pieces
FIND_PACKAGE(CMSIS 3.20 REQUIRED EXACT)
FIND_PACKAGE(STM32L1 REQUIRED)
FIND_PACKAGE(STM32L1Drivers REQUIRED)

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -nostdlib")
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -nostdlib")
SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -nostdlib")

# List of files to add during compilation
SET(TOOLCHAIN_SOURCES ${STM32L1Drivers_SOURCES} ${STM32L1_SOURCES} "${CMAKE_CURRENT_LIST_DIR}/../startup/stm32l1.S" CACHE STRING "Toolchain sources")

# Linker script template for this CPU
SET(LINKER_SCRIPT_TEMPLATE "${CMAKE_CURRENT_LIST_DIR}/../ld/stm32l1.ld.in" CACHE STRING "Linker script template")

# Include generic chip family cmake script
INCLUDE("${CMAKE_CURRENT_LIST_DIR}/cortex-m3.include")

