.include "cortex-m0base.S"

.section .kinetis_cfg,"a",%progbits
    .byte 0xff // backdoor key
    .byte 0xff
    .byte 0xff
    .byte 0xff
    .byte 0xff
    .byte 0xff
    .byte 0xff
    .byte 0xff
    .byte 0xff
    .byte 0xff
    .byte 0xff
    .byte 0xff
    .byte 0xff // reserved
    .byte 0xff // no flash protection
    .byte 0b11111110 // security disabled
    .byte 0xff // non volatile

/* Put everything in a section called ".isr_vector" from now on... */
.section .isr_vector,"a",%progbits
    ISR_RESERVED
    ISR_RESERVED
    ISR_RESERVED
    ISR_RESERVED
    ISR_RESERVED
    ISR FTMRE_IRQHandler, defaultExceptionHandler                    // Command complete
    ISR PMC_IRQHandler, defaultExceptionHandler                      // Low voltage warning
    ISR IRQ_IRQHandler, defaultExceptionHandler                      // External interrupt
    ISR I2C0_IRQHandler, defaultExceptionHandler                     // I2C all sources
    ISR_RESERVED
    ISR SPI0_IRQHandler, defaultExceptionHandler                     // SPI0 all sources
    ISR_RESERVED
    ISR UART0_IRQHandler, defaultExceptionHandler                    // Status and error
    ISR_RESERVED
    ISR_RESERVED
    ISR ADC0_IRQHandler, defaultExceptionHandler                     // Conversion complete
    ISR ACMP0_IRQHandler, defaultExceptionHandler                    // Analog comparator 0 interrupt
    ISR FTM0_IRQHandler, defaultExceptionHandler                     // FTM0 all sources
    ISR_RESERVED
    ISR FTM2_IRQHandler, defaultExceptionHandler                     // FTM2 all sources
    ISR RTC_IRQHandler, defaultExceptionHandler                      // RTC overflow
    ISR ACMP1_IRQHandler, defaultExceptionHandler                    // Analog comparator 1 interrupt
    ISR PIT_CH0_IRQHandler, defaultExceptionHandler                  // PIT CH0 overflow
    ISR PIT_CH1_IRQHandler, defaultExceptionHandler                  // PIT CH1 overflow
    ISR KBI0_IRQHandler, defaultExceptionHandler                     // Keyboard interrupt 0
    ISR KBI1_IRQHandler, defaultExceptionHandler                     // Keyboard interrupt 1
    ISR_RESERVED
    ISR ICS_IRQHandler, defaultExceptionHandler                      // Clock loss of lock
    ISR WDOG_IRQHandler, defaultExceptionHandler                     // Watchdog timeout
    ISR PWT_IRQHandler, defaultExceptionHandler                      // PWT all sources
    ISR_RESERVED
    ISR_RESERVED

