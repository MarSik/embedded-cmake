        /* The code section starts here: everything from now on is part of
           the .text section  */
        .text

        .align                                          /* Make sure address is aligned for code output */

        /* Default startup code for initializing the memory and the clock */
        FUNCTION            Reset_Handler
        .global Reset_Handler
        .weak Reset_Handler

        ldr                 r0,=LowLevelInit            /* get the address of the LowLevelInit routine */

        cmp                 r0,#0                       /* is it NULL ? */

        beq                 noinit
        blx                 r0                          /* if not, call the routine. */

noinit: 


        /* copy the .data section from Flash memory to SRAM (this allows us to pre-initialize variables) */

        ldr                 r1,=_sidata                 /* point our source register to the start of the .data section (in FLASH) */

        ldr                 r2,=_sdata                  /* point our destination register to the SRAM dedicated to our writable data (in SRAM) */

        ldr                 r3,=_edata                  /* point r3 after the last byte we will be writing (in SRAM) */

        bl                  copy                        /* call a subroutine that copies the memory block */

        /* copy code from Flash memory to SRAM. Code executes faster in SRAM than in Flash memory. */

        ldr                 r1,=_sifastcode             /* source address (in FLASH) */

        ldr                 r2,=_sfastcode              /* destination start (in SRAM) */

        ldr                 r3,=_efastcode              /* destination end (in SRAM) */

        bl                  copy                        /* copy code */
 

        /* Now zero the .bss section. This is a section of memory, which is dedicated to our varaibles. */

        movs                r0,#0                       /* zero r0 */

        ldr                 r1,=_sbss                   /* point r1 to BSS starting address (in SRAM) */

        ldr                 r2,=_ebss                   /* point r2 to BSS ending address (in SRAM) */

1:      cmp                 r1,r2                       /* check if end is reached */

        beq                 endbss
        str                 r0,[r1]
        adds                r1, r1, #4                  /* if end not reached, store zero and advance pointer */

        b                   1b                          /* if end not reached, branch back to loop */
endbss:

        ldr                 r1,=__init_start            /* execute static constructors */
        ldr                 r2,=__init_end
        bl                  callinits

        ldr                 r1,=__init_array_start
        ldr                 r2,=__init_array_end
        bl                  callinits
 

        ldr                 r0,=SystemInit              /* get the address of the SystemInit routine */

        cmp                 r0,#0                       /* is it NULL ? */

        beq                 nosysinit
        blx                 r0                          /* if not, call the routine. */

nosysinit:

        /* TODO execute .init and .init_array functions */
 

        ldr                 r0,=main                    /* get the address of the C 'main' code */

        blx                 r0                          /* jump to code */

        b                   defaultExceptionHandler     /* if we ever get here, we'll continue into an infinite loop for safety reasons */

        ENDFUNC             Reset_Handler


        FUNCTION copy

        cmp                 r2,r3                       /* check if we've reached the end */

        beq                 endcopy

        ldr                 r0,[r1]                     /* if end not reached, get word and advance source pointer */
        adds                r1, r1, #4

        str                 r0,[r2]                     /* if end not reached, store word and advance destination pointer */
        adds                r2, r2, #4

        blo                 copy                        /* end not reached, branch back to loop */

endcopy:
        bx                  lr                          /* return to caller */

        ENDFUNC             copy

        FUNCTION callinits

        cmp                 r1,r2                       /* check if we've reached the end */

        beq                 endcallinits

        ldr                 r0,[r1]                     /* if end not reached, get address, advance pointer and call the init function */
        adds                r1, r1, #4

        blx                 r0

        blo                 callinits                   /* end not reached, branch back to loop */

endcallinits:
        bx                  lr                          /* return to caller */

        ENDFUNC             callinits

